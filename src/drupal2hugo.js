import fs from "fs";
import fsExtra from "fs-extra";
import crypto from "crypto";

export class Drupal2Hugo {
  constructor(drupal, baseDir, custom, options) {
    this.drupal = drupal;
    this.baseDir = baseDir;
    this.custom = custom;
    this.options = options || {};
    this.hashValues = {};
    this.fileHashValues = this.options.cache?.get("fileHashValues")?.value || {};
  }

  md5Hex(str) {
    const md5 = crypto.createHash("md5")
    return md5.update(str, "binary").digest("hex");
  }

  isUnchanged(dir, fileName, content) {
    return this.fileHashValues[dir + fileName] == this.md5Hex(content);
  }
  
  setHashValue(dir, fileName, content) {
    this.fileHashValues[dir + fileName] = this.md5Hex(content);
  }
  
  setCache() {
    this.options.cache?.set("fileHashValues", this.fileHashValues, Date.now());
  }

  /*
    Convert data to a list of data
   */
  toList(data) {
    if (data == null)
      return [];
    if (Array.isArray(data))
      return data;
    return [data];
  }

  /*
    Convert alias to URL
   */
  aliasToUrl(alias) {
    if (alias == null)
      return null;
    return alias.replace(/^\//, "").replace(/\/?$/, "/");
  }

  /*
    Extract the fields except taxonomies from a node
   */
  getFields(node) {
    let fields = {};
    for (const key in node.attributes) {
      if (key == "body") {
        fields[key] = node.attributes[key];
      } else if (key.startsWith("field_")) {
        fields[key.substring("field_".length)] = node.attributes[key];
      }
    }
    for (const key in node.relationships) {
      if (key.startsWith("field_")) {
        const name = key.substring("field_".length);
        let data = node.relationships[key].data;
        if (data == null) {
        } else if (Array.isArray(data)) {
          data = data.filter(d => d.type && ! d.type.startsWith("taxonomy_term--"));
          if (data.length > 0)
            fields[name] = data;
        } else if (data.type && ! data.type.startsWith("taxonomy_term--")) {
          fields[name] = data;
        }
      }
    }
    return fields;
  }

  /*
    File data
   */
  async fileMap(langcode) {
    const map = {};
    for (const data of await this.drupal.getFiles(langcode)) {
      const id = data.attributes?.drupal_internal__fid || 0;
      if (id) {
        map[id] = { attributes: data.attributes };
      }
    }
    return map;
  }

  /*
    Media data
   */
  async mediaMap(langcode) {
    const map = {};
    for (const name of await this.drupal.getMediaNames(langcode)) {
      map[`media--${name}`] = {};
      for (const data of await this.drupal.getMediaNodes(langcode, name)) {
        const id = data.attributes?.drupal_internal__mid || 0;
        if (id) {
          const fields = this.getFields(data);
          map[data.type][id] = { attributes: data.attributes, fields };
        }
      }
    }
    return map;
  }

  /*
    Block data
   */
  async blockMap(langcode) {
    const map = {};
    for (const name of await this.drupal.getBlockNames(langcode)) {
      map[`block_content--${name}`] = {};
      for (const data of await this.drupal.getBlockNodes(langcode, name)) {
        const id = data.attributes?.drupal_internal__id || 0;
        if (id) {
          const fields = this.getFields(data);
          map[data.type][id] = { attributes: data.attributes, fields };
        }
      }
    }
    return map;
  }

  async resolveField(langcode, name, field, maps, errors) {
    const id = field.meta?.drupal_internal__target_id || 0;
    let f = null
    if (! field.type) {
      f = field;
    } else if (! id) {
      const msg = `Unknown id in field ${name}`;
      errors.push(msg);
    } else if (field.type.startsWith("node--")) {
      const name = field.type.substring("node--".length);
      f = { type: field.type, name, nid: id, reference: `/${name}/${id}` };
    } else if (field.type == "file--file") {
      f = { type: field.type, meta: field.meta, url: maps.fileMap[id]?.attributes?.uri?.url };
    } else if (field.type.startsWith("media--")) {
      let d = maps.mediaMap[field.type][id];
      if (d.fields) {
        const fields = await this.resolveFields(langcode, d.fields, maps, errors);
        f = { type: field.type, fields };
      } else {
        f = {};
      }
    } else if (field.type.startsWith("block_content--")) {
      let d = maps.blockMap[field.type][id];
      if (d.fields) {
        const fields = await this.resolveFields(langcode, d.fields, maps, errors);
        f = { type: field.type, fields };
      } else {
        f = {};
      }
    } else {
      const msg = `Unknown type in field ${name}`;
      errors.push(msg);
      f = field;
    }
    return f;
  }

  async resolveFields(langcode, fields, maps, errors) {
    let fs = {};
    for (const name in fields) {
      const field = fields[name];
      let f = null;
      if (Array.isArray(field)) {
        f = [];
        for (const field1 of field) {
          f.push(await this.resolveField(langcode, name, field1, maps, errors));
        }
      } else if (field != null) {
        f = await this.resolveField(langcode, name, field, maps, errors);
      }
      if (f)
        fs[name] = f;
    }
    return fs;
  }

  getReferences(fields) {
    let references = {};
    for (const name in fields) {
      for (const field of this.toList(fields[name])) {
        if (field.reference) {
          if (! references[field.name])
            references[field.name] = [];
          references[field.name].push(field.reference);
        }
      }
    }
    return references;
  }

  getHugoField(fields) {
    let hugo = null;
    if (fields.hugo)
      hugo = JSON.parse(fields.hugo);
    if (! hugo)
      hugo = {};
    return hugo;
  }

  getNodeTaxonomies(node) {
    const taxonomies = {};
    const rel = node.relationships;
    for (const r in rel) {
      for (const d of this.toList(rel[r].data)) {
        let t = d.type || "";
        const tid = d.meta?.drupal_internal__target_id || 0;
        if (t.startsWith("taxonomy_term--") && tid) {
          t = t.substring("taxonomy_term--".length);
          if (! taxonomies[t]) {
            taxonomies[t] = [];
          }
          const word = `${t}-${tid}`;
          taxonomies[t].push(word);
        }
      }
    }
    return taxonomies;
  }

  async getMaps(langcode) {
    const fileMap = await this.fileMap(langcode);
    const mediaMap = await this.mediaMap(langcode);
    const blockMap = await this.blockMap(langcode);
    return { fileMap, mediaMap, blockMap };
  }

  async getPages(langcode) {
    const maps = await this.getMaps(langcode);
    const names = await this.drupal.getNodeTypes(langcode, { nameOnly: true });
    const pages = {};
    for (const name of names) {
      const nodes = await this.drupal.getNodes(langcode, name);
      for (const node of nodes) {
        if (! this.custom.langMatch(node, langcode))
          continue;
        const nid = node.attributes?.drupal_internal__nid || 0;
        const key = `/${name}/${nid}`;
        const body = (node.attributes.body?.value || "").replace(/\r\n/g, "\n");
        const url = this.aliasToUrl(node.attributes?.path?.alias);
        const title = node.attributes?.title || "";
        const taxonomies = this.getNodeTaxonomies(node);
        const errors = [];
        let fields = this.getFields(node);
        fields = await this.resolveFields(langcode, fields, maps, errors);
        delete fields.body;
        const references = this.getReferences(fields);
        let frontMatter = {
          key, nid, name, url, title,
          ...taxonomies, fields, references, errors,
        };
        if (this.custom.includeHugoFrontMatter) {
          const hugo = this.getHugoField(fields);
          if (hugo) {
            frontMatter = { ...frontMatter, ...hugo };
            delete fields.hugo;
          }
        }
        if (this.custom.includeOriginalJson)
          frontMatter = { ...frontMatter, node };
        let dir = `${langcode}/${name}/`;
        let fileName = `${nid}.html`;
        if (this.custom.alwaysCreateSectionPages) {
          dir = `${langcode}/${name}/${nid}/`;
          fileName = "_index.html";
          frontMatter.type = `${name}-single`;
        }
        pages[key] = { dir, fileName, frontMatter, body, node };
        for (const msg of errors) {
          console.error(`ERROR: ${msg} at /${dir}${fileName} (${frontMatter.title})`);
        }
      }
    }
    if (this.custom.includeReferrers) {
      for (const key in pages) {
        const name = pages[key].frontMatter.name;
        const refs = pages[key].frontMatter.references || [];
        for (const name1 in refs) {
          for (const key1 of refs[name1]) {
            if (pages[key1] == null) {
              const msg = `ERROR: Undefined reference ${key1} at /${pages[key].dir}${pages[key].fileName} (${pages[key].frontMatter.title})`;
              pages[key].frontMatter.errors.push(msg);
              console.error(msg);
              continue;
            }
            if (pages[key1].frontMatter == null)
              pages[key1].frontMatter = {};
            if (pages[key1].frontMatter.referrers == null)
              pages[key1].frontMatter.referrers = {};
            if (pages[key1].frontMatter.referrers[name] == null)
              pages[key1].frontMatter.referrers[name] = [];
            pages[key1].frontMatter.referrers[name].push(key);
          }
        }
      }
    }
    return pages;
  }

  /*
    Returns destination directory
  */
  destinationDir() {
    const hugoDir = this.custom.hugoDir || "content";
    if (! this.baseDir)
      return hugoDir;
    return this.baseDir + "/" + hugoDir;
  }

  /*
    Delete directory
  */
  emptyDir() {
    fsExtra.emptyDirSync(this.destinationDir());
  }

  /*
    Write file
  */
  writeFile(dir, fileName, content) {
    dir = dir.replace(/\/?$/, "/");
    fileName = fileName.replace(/^\//, "");
    const dir1 = `${this.destinationDir()}/${dir}/`.replace(/\/\/$/, "/");
    if (fs.existsSync(dir1 + fileName) && this.isUnchanged(dir, fileName, content)) {
      return 0;
    }
    try {
      fs.mkdirSync(dir1, { recursive: true });
      fs.writeFileSync(dir1 + fileName, content);
      this.setHashValue(dir, fileName, content);
    } catch (err) {
      console.error(err);
      return 0;
    }
    return 1;
  }

  async writeSectionPages(langcode) {
    let total = 0;
    let count = 0;
    const time0 = Date.now();
    const nodeTypes = await this.drupal.getNodeTypes(langcode);
    for (const node of nodeTypes) {
      const name = node.attributes.drupal_internal__type;
      const key = `/${name}/`;
      const title = node.attributes.name;
      const body = "";
      let frontMatter = {
        key, name, title
      };
      if (this.custom.includeOriginalJson)
        frontMatter = { ...frontMatter, node };
      const dir = `${langcode}/${name}/`;
      const fileName = "_index.html";
      const result = this.custom.sectionPageHook(dir, fileName, frontMatter, body, node);
      if (result != null) {
        const content = JSON.stringify(result.frontMatter, null, 2) + "\n\n" + result.body;
        count += this.writeFile(result.dir, result.fileName, content);
        total++;
      }
    }
    const time = Date.now() - time0;
    if (this.options.verbose >= 0)
      console.error(`Drupal2Hugo: Wrote ${count}/${total} section pages of ${langcode} (${time/100} s)`);
    return [count, total];
  }

  writeSubsectionPage(langcode, name, parentDir, parentFrontMatter) {
    let total = 0;
    let count = 0;
    const dir = parentDir + "/" + name;
    const fileName = "_index.html";
    const references = {};
    if (parentFrontMatter.references)
      references[name] = parentFrontMatter.references[name];
    const referrers = {};
    if (parentFrontMatter.referrers)
      referrers[name] = parentFrontMatter.referrers[name];
    if (references[name] || referrers[name]) {
      const key = parentFrontMatter.key + "/" + name + "/";
      const type = `${name}-subsection`;
      const frontMatter = { key, name, title: type, type, references, referrers };
      if (parentFrontMatter.url)
        frontMatter.url = parentFrontMatter.url + name + "/";
      const result = this.custom.subsectionPageHook(dir, fileName, frontMatter, "");
      if (result != null) {
        const content = JSON.stringify(result.frontMatter, null, 2) + "\n\n" + result.body;
        count += this.writeFile(result.dir, result.fileName, content);
        total++;
      }
    }
    return [count, total];
  }

  async writeSinglePages(langcode) {
    let total = 0;
    let count = 0;
    let total1 = 0;
    let count1 = 0;
    const time0 = Date.now();
    const pages = await this.getPages(langcode);
    for (const key in pages) {
      const page = pages[key];
      const result = this.custom.singlePageHook(page.dir, page.fileName, page.frontMatter, page.body, page.node);
      if (result != null) {
        if (result.subsections) {
          // Create subsection pages
          let names = Object.keys({ ...result.frontMatter.references, ...result.frontMatter.referrers });
          if (Array.isArray(result.subsections))
            names = result.subsections;
          for (const name of names) {
            const [c1, t1] = await this.writeSubsectionPage(langcode, name, result.dir, result.frontMatter);
            count1 += c1;
            total1 += t1;
          }
        }
        const content = JSON.stringify(result.frontMatter, null, 2) + "\n\n" + result.body;
        count += this.writeFile(result.dir, result.fileName, content);
        total++;
      }
    }
    const time = Date.now() - time0;
    if (this.options.verbose >= 0) {
      console.error(`Drupal2Hugo: Wrote ${count}/${total} single pages (${count1}/${total1} subsection pages) of ${langcode} (${time/1000} s)`);
    }
    return [count, total];
  }

  async writeTaxonomyPages(langcode) {
    let total = 0;
    let count = 0;
    const time0 = Date.now();
    const taxonomies = await this.drupal.getTaxonomies(langcode);
    for (const node of taxonomies) {
      const name = node.attributes.drupal_internal__vid;
      const key = `/${name}/`;
      const title = node.attributes.name;
      const body = "";
      let frontMatter = {
        key, name, title
      };
      if (this.custom.includeOriginalJson)
        frontMatter = { ...frontMatter, node };
      const dir = `${langcode}/${name}/`;
      const fileName = "_index.html";
      const result = this.custom.taxonomyPageHook(dir, fileName, frontMatter, body, node);
      if (result != null) {
        const content = JSON.stringify(result.frontMatter, null, 2) + "\n\n" + result.body;
        count += this.writeFile(result.dir, result.fileName, content);
        total++;
      }
    }
    const time = Date.now() - time0;
    if (this.options.verbose >= 0)
      console.error(`Drupal2Hugo: Wrote ${count}/${total} taxonomy pages of ${langcode} (${time/1000} s)`);
    return [count, total];
  }

  async writeTermPages(langcode) {
    let total = 0;
    let count = 0;
    const time0 = Date.now();
    const maps = await this.getMaps(langcode);
    const names = await this.drupal.getTaxonomies(langcode, { nameOnly: true });
    for (const name of names) {
      const nodes = await this.drupal.getTerms(langcode, name);
      for (const node of nodes) {
        const tid = node.attributes.drupal_internal__tid;
        const word = `${name}-${tid}`;
        const key = `/${name}/${tid}`;
        const url = this.aliasToUrl(node.attributes?.path?.alias);
        let title = node.attributes.name;
        if (! this.custom.langMatch(node, langcode))
          title = word;
        const errors = [];
        let fields = this.getFields(node);
        fields = await this.resolveFields(langcode, fields, maps, errors);
        delete fields.body;
        const body = node.attributes.description?.processed || "";
        const dir = `${langcode}/${name}/${word}/`;
        const fileName = "_index.html";
        let frontMatter = { key, tid, name, word, url, title, fields, errors };
        if (this.custom.includeHugoFrontMatter) {
          const hugo = this.getHugoField(fields);
          if (hugo) {
            frontMatter = { ...frontMatter, ...hugo };
            delete fields.hugo;
          }
        }
        if (this.custom.includeOriginalJson)
          frontMatter = { ...frontMatter, node };
        const result = this.custom.termPageHook(dir, fileName, frontMatter, body, node);
        if (result != null) {
          const content = JSON.stringify(result.frontMatter, null, 2) + "\n\n" + result.body;
          count += this.writeFile(result.dir, result.fileName, content);
          total++;
          for (const msg of errors) {
            console.error(`ERROR: ${msg} at /${result.dir}${result.fileName} (${result.frontMatter.title})`);
          }
        }
      }
    }
    const time = Date.now() - time0;
    if (this.options.verbose >= 0)
      console.error(`Drupal2Hugo: Wrote ${count}/${total} taxonomy term pages of ${langcode} (${time/1000} s)`);
    return [count, total];
  }

  async printConfig(langcode) {
    console.log("[taxonomies]");
    const taxonomies = await this.drupal.getTaxonomies(langcode);
    for (const node of taxonomies) {
      const name = node.attributes.drupal_internal__vid;
      console.log(`${name} = "${name}"`);
    }
    console.log("");
  }

  async printDict(langcode) {
    // TODO
  }

}
