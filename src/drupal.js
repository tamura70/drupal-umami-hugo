import fetch from "node-fetch";

export class Drupal {
  constructor(baseUrl, request, custom, options) {
    this.baseUrl = baseUrl;
    this.request = request;
    this.custom = custom;
    this.options = options || {};
    this.timestampMargin = 10; // 10 seconds
  }

  getCache(key) {
    return this.options.cache?.get(key);
  }
  
  setCache(key, value, timestamp) {
    if (! this.options.cache)
      return;
    this.options.cache.set(key, value, timestamp);
  }
  
  getJson(langcode = "", path = "") {
    const code = langcode ? `${langcode}/` : "";
    const url = `${this.baseUrl}/${code}jsonapi${path}`;
    if (this.options.verbose >= 2)
      console.error({ url });
    const time0 = Date.now();
    return fetch(url, this.request).then(response => {
      const time = Date.now() - time0;
      if (this.options.verbose > 0)
        console.error({ time, langcode, path });
      if (! response.ok)
        return {};
      const json = response.json();
      return json;
    });
  }

  getJsonDataParallel(langcode, path, start, parallel) {
    const code = langcode ? `${langcode}/` : "";
    const limit = 50;
    const url0 = `${this.baseUrl}/${code}jsonapi${path}`;
    const list = [];
    const ps = Array.from(Array(parallel), (v,k) => k).map(i => {
      let url = url0;
      if (start+i > 0) {
        if (url.includes("?")) {
          url = url.replace("?", `?page[offset]=${limit*(start+i)}&`);
        } else {
          url += `?page[offset]=${limit*(start+i)}`;
        }
      }
      if (this.options.verbose >= 2)
        console.error({ url });
      return fetch(url, this.request)
        .then(response => {
          if (! response.ok)
            return {};
          return response.json();
        })
        .then(json => {
          if (json.meta) {
            console.error({ error:"ERROR", url, meta:json.meta });
          }
          list[i] = json.data;
        });
    });
    return Promise.allSettled(ps).then(() => list);
  }

  async getJsonDataUpdate(langcode, path, timestamp) {
    const ts = Math.floor(timestamp/1000) - this.timestampMargin;
    path = path + "?sort=-changed&filter[a][condition][path]=changed&filter[a][condition][operator]=%3E&filter[a][condition][value]=" + ts;
    if (this.options.verbose >= 2)
      console.error({ langcode, path })
    const time0 = Date.now();
    let start = 0;
    const parallel = 2;
    let allList = [];
    while (true) {
      const list = await this.getJsonDataParallel(langcode, path, start, parallel);
      allList.push(list.flat());
      start += parallel;
      if (! list[list.length-1] || list[list.length-1].length == 0)
        break;
    }
    const result = allList.flat();
    const time = Date.now() - time0;
    const count = result.length;
    if (this.options.verbose > 0)
      console.error({ count, time, langcode, path, timestamp });
    return result;
  }

  async getJsonDataIncremental(langcode, path, opts = {}) {
    const time0 = Date.now();
    const cacheKey = ["jsonData", langcode, path];
    if (! this.getCache(cacheKey))
      return null;
    if (this.options.verbose >= 2)
      console.error(`Cache hit ${cacheKey}`);
    const result = this.getCache(cacheKey).value;
    let updates = await this.getJsonDataUpdate(langcode, path, this.getCache(cacheKey).timestamp);
    updates = updates.filter(d => d);
    if (updates.length > 0) {
      console.error(`Drupal2Hugo: Detected ${updates.length} updates for ${langcode}${path}`);
    }
    const map = Object.fromEntries(updates.map(d => [d.id, d]));
    for (let i = 0; i < result.length; i++) {
      const id = result[i]?.id;
      if (id && map[id]) {
        result[i] = map[id];
        delete map[id];
      }
    }
    for (const id in map)
      result.push(map[id]);
    this.setCache(cacheKey, result, time0);
    return result;
  }

  async getJsonData(langcode, path, opts = {}) {
    const time0 = Date.now();
    const cacheKey = ["jsonData", langcode, path];
    if (opts.incremental && this.getCache(cacheKey)) {
      return await this.getJsonDataIncremental(langcode, path, opts);
    }
    let start = 0;
    const parallel = this.custom.parallel(langcode, path);
    let allList = [];
    while (true) {
      const list = await this.getJsonDataParallel(langcode, path, start, parallel);
      allList.push(list.flat());
      start += parallel;
      if (! list[list.length-1] || list[list.length-1].length == 0)
        break;
    }
    const result = allList.flat();
    const time = Date.now() - time0;
    const count = result.length;
    if (this.options.verbose > 0)
      console.error({ count, time, langcode, path });
    this.setCache(cacheKey, result, time0);
    return result;
  }

  /*
    Returns the list of all langcodes
   */
  async getLangcodes() {
    const json = await this.getJson("", "/configurable_language/configurable_language");
    let langcodes = [];
    for (const d of json.data) {
      if (d.type == "configurable_language--configurable_language") {
        const langcode = d.attributes?.drupal_internal__id;
        if (langcode && langcode != "und" && langcode != "zxx")
          langcodes.push(langcode);
      }
    }
    return langcodes;
  }

  /*
    Returns the list of all node types
   */
  async getNodeTypes(langcode, opts = {}) {
    let json = await this.getJsonData(langcode, "/node_type/node_type");
    json = json.filter(node => node?.type == "node_type--node_type");
    return opts.nameOnly ? json.map(node => node.attributes.drupal_internal__type) : json;
  }

  /*
    Returns the list of all nodes with the specified node type name
   */
  async getNodes(langcode, name) {
    const json = await this.getJsonData(langcode, `/node/${name}`, { incremental: true });
    return json.filter(node => node != null);
  }

  /*
    Returns the list of all taxonomies (vocabularies)
   */
  async getTaxonomies(langcode, opts = {}) {
    let json = await this.getJsonData(langcode, "/taxonomy_vocabulary/taxonomy_vocabulary");
    json = json.filter(node => node?.type == "taxonomy_vocabulary--taxonomy_vocabulary");
    return opts.nameOnly ? json.map(node => node.attributes.drupal_internal__vid) : json;
  }

  /*
    Returns the list of all terms with the specified vocabulary name
   */
  async getTerms(langcode, name) {
    const json = await this.getJsonData(langcode, `/taxonomy_term/${name}`, { incremental: true });
    return json.filter(node => node != null);
  }

  /*
    Returns the list of all file data
   */
  async getFiles(langcode) {
    let files = {};
    const fileData = await this.getJsonData(langcode, `/file/file`, { incremental: true });
    return fileData.filter(data => data && data.type == "file--file");
  }

  /*
    Returns the list of all media type names
   */
  async getMediaNames(langcode) {
    const json = await this.getJson(langcode);
    const names = [];
    for (const key in json.links) {
      if (key.startsWith("media--")) {
        names.push(key.substring("media--".length));
      }
    }
    return names;
  }

  /*
    Returns the list of all nodes with the specified media type name
   */
  async getMediaNodes(langcode, name) {
    const json = await this.getJsonData(langcode, `/media/${name}`, { incremental: true });
    return json.filter(node => node != null && node.type == `media--${name}`);
  }

  /*
    Returns the list of all block type names
   */
  async getBlockNames(langcode) {
    const json = await this.getJson(langcode);
    const names = [];
    for (const key in json.links) {
      if (key.startsWith("block_content--")) {
        names.push(key.substring("block_content--".length));
      }
    }
    return names;
  }

  /*
    Returns the list of all nodes with the specified block type name
   */
  async getBlockNodes(langcode, name) {
    const json = await this.getJsonData(langcode, `/block_content/${name}`, { incremental: true });
    return json.filter(node => node != null && node.type == `block_content--${name}`);
  }

}
