import fs from "fs";
import fsExtra from "fs-extra";

export class Cache {
  constructor(cacheDir, options) {
    cacheDir = cacheDir.replace(/\/?$/, "");
    fs.mkdirSync(cacheDir, { recursive: true });
    this.cacheDir = cacheDir;
    this.data = {};
    this.options = options || {};
  }

  clear() {
    this.data = {};
  }

  toKey(key) {
    let k;
    if (Array.isArray(key)) {
      k = key.join("--");
    } else if (key == key.toString()) {
      k = key;
    } else {
      return encodeURIComponent(JSON.stringify(key));
    }
    return k.replaceAll("/", "--");
  }

  set(key, value, timestamp = Date.now()) {
    this.data[this.toKey(key)] = { value, timestamp };
  }

  get(key) {
    return this.data[this.toKey(key)];
  }

  emptyDir() {
    fsExtra.emptyDirSync(this.cacheDir);
  }

  save() {
    this.emptyDir();
    let c = 0;
    for (const k of Object.keys(this.data)) {
      const fileName = this.cacheDir + "/" + k + ".json";
      fs.writeFileSync(fileName, JSON.stringify(this.data[k]));
      c++;
    }
    return c;
  }

  load() {
    this.clear();
    let c = 0;
    for (const fileName of fs.readdirSync(this.cacheDir)) {
      if (fileName.endsWith(".json")) {
        const k = fileName.replace(/^.*\//, "").replace(/\.\w+$/, "")
        const content = fs.readFileSync(this.cacheDir + "/" + fileName);
        this.data[k] = JSON.parse(content);
        c++;
      }
    }
    return c;
  }
}
