import { Cache } from "./cache.js";
import { Drupal } from "./drupal.js";
import { Drupal2Hugo } from "./drupal2hugo.js";
import fetch from "node-fetch";
import dotenv from "dotenv";
import {setTimeout} from "timers/promises";

function loadCache(cache) {
  const c = cache.load();
  console.error(`Drupal2Hugo: Loaded ${c} cache files from '${cache.cacheDir}'`);
}

function saveCache(cache) {
  const c = cache.save();
  console.error(`Drupal2Hugo: Saved ${c} cache files to '${cache.cacheDir}'`);
}

async function main() {
  let baseDir = ".";
  let verbose = false;
  let cacheDir = "d2h_cache";
  let clearCache = true;
  let clearContents = true;
  let watchInterval = 0;
  let args = [];
  for (let i = 2; i < process.argv.length; ) {
    const arg = process.argv[i++];
    if (arg == "-v") {
      verbose++;
    } else if (arg == "-b") {
      baseDir = process.argv[i++];
    } else if (arg == "-c") {
      cacheDir = process.argv[i++];
    } else if (arg == "-i") {
      clearCache = false;
      clearContents = false;
    } else if (arg == "-w") {
      watchInterval = +process.argv[i++];
    } else if (arg.startsWith("-")) {
      console.error("Unknown option " + arg);
    } else {
      args.push(arg);
    }
  }
  baseDir = baseDir.replace(/\/$/, "");

  const dir = baseDir.startsWith("/") ? baseDir : process.cwd() + "/" + baseDir;
  const custom = await import(dir + "/custom.mjs");
  dotenv.config({ path: dir + "/.env" });
  const drupalUrl = process.env.DRUPAL_URL;
  let request;
  if (process.env.DRUPAL_APIKEY) {
    const apikey = process.env.DRUPAL_APIKEY;
    request = { method: "GET", headers: {"api-key": apikey} };
  } else if (process.env.DRUPAL_USER) {
    // TODO
    const user = process.env.DRUPAL_USER;
    const password = process.env.DRUPAL_PASSWORD;
    const url = drupalUrl + "/session/token";
    const token = await fetch(url, { method: "GET" }).then(response => response.text());
    request = {
      method: "GET",
      headers: {
        "Authorization": "Basic " + btoa(`${user}:${password}`),
        "X-CSRF-Token": token
      }
    };
  }
  if (args.length == 1 && args[0] == "download") {
    const cache = new Cache(`${baseDir}/${cacheDir}`);
    console.error(`Drupal2Hugo: Cache directory is '${cache.cacheDir}'`);
    if (clearCache) {
      console.error(`Drupal2Hugo: Clearing cache '${cache.cacheDir}'`);
      cache.emptyDir();
    } else {
      loadCache(cache);
    }
    const drupal = new Drupal(drupalUrl, request, custom, { verbose, cache });
    const d2h = new Drupal2Hugo(drupal, baseDir, custom, { verbose, cache });
    console.error(`Drupal2Hugo: Content directory is '${d2h.destinationDir()}'`);
    if (clearContents) {
      console.error(`Drupal2Hugo: Clearing content '${d2h.destinationDir()}'`);
      d2h.emptyDir();
    }
    while (true) {
      const time0 = Date.now();
      let total = 0;
      let count = 0;
      const langcodes = await drupal.getLangcodes();
      for (const langcode of langcodes) {
        const [c1, t1] = await d2h.writeSectionPages(langcode);
        const [c2, t2] = await d2h.writeSinglePages(langcode);
        const [c3, t3] = await d2h.writeTaxonomyPages(langcode);
        const [c4, t4] = await d2h.writeTermPages(langcode);
        count += c1 + c2+ c3 + c4;
        total += t1 + t2+ t3 + t4;
      }
      const time = Date.now() - time0;
      console.error(`Drupal2Hugo: Wrote ${count}/${total} pages in total (${time/1000} s)`);
      d2h.setCache();
      saveCache(cache);
      if (! watchInterval)
        break;
      console.error(`Drupal2Hugo: Waiting ${watchInterval} seconds`);
      await setTimeout(watchInterval*1000);
    }
  } else if (args.length == 1 && args[0] == "config") {
    const drupal = new Drupal(drupalUrl, request, custom, { verbose });
    const d2h = new Drupal2Hugo(drupal, baseDir, custom, { verbose });
    await d2h.printConfig();
  } else if (args.length >= 2 && args[0] == "api") {
    const drupal = new Drupal(drupalUrl, request, custom, { verbose });
    const langcode = args[1];
    const typ = args[2];
    const name = args[3];
    let json = "";
    if (! langcode) {
      json = await drupal.getJson();
    } else if (! typ) {
      json = await drupal.getJson(langcode);
    } else if (typ == "lang") {
      json = await drupal.getLangcodes();
    } else if (typ == "node") {
      if (! name) {
        json = await drupal.getNodeTypes(langcode);
      } else {
        json = await drupal.getNodes(langcode, name);
      }
    } else if (typ == "term") {
      if (! name) {
        json = await drupal.getTaxonomies(langcode);
      } else {
        json = await drupal.getTerms(langcode, name);
      }
    } else if (typ == "file") {
      json = await drupal.getFiles(langcode);
    } else if (typ == "media") {
      if (! name) {
        json = await drupal.getMediaNames(langcode);
      } else {
        json = await drupal.getMediaNodes(langcode, name);
      }
    } else if (typ == "block") {
      if (! name) {
        json = await drupal.getBlockNames(langcode);
      } else {
        json = await drupal.getBlockNodes(langcode, name);
      }
    }
    console.log(JSON.stringify(json, null, 2));
  } else if (args.length == 2 && args[0] == "url") {
    const url = args[1];
    const json = await fetch(url, request).then(response => response.json());
    console.log(JSON.stringify(json, null, 2));
  } else {
    console.error("Example usage");
    console.error("$ node src/main.js [-b baseDir] [-i] download");
    console.error("$ node src/main.js [-b baseDir] config");
    console.error("$ node src/main.js [-b baseDir] api langcode [lang,node,term,file,media,block] [name]");
    console.error("$ node src/main.js [-b baseDir] url http://localhost:8881/ja/jsonapi");
  }
}

main();

