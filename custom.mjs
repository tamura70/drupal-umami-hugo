/*
  The max number of concurrent accesses to get node data from Drupal
*/
export function parallel(langcode, path) {
  if (path.startsWith("/node/"))
    return 10;
  else if (path.startsWith("/taxonomy_term//"))
    return 3;
  return 2;
};

/*
  Hugo content directory
*/
export const hugoDir = "content";

/*
  Check langcode for translation pages
  - strict=false: default language page is used when the translation is unavailable
  - strict=true: no pages are generated when the translation is unavailable
 */
export function langMatch(node, langcode) {
  const strict = false;
  // const strict = true;
  const lang = node.attributes?.langcode
  if (strict)
    return lang == "und" || lang == langcode;
  return true;
}

/*
  Boolean flag to include hugo field in the front matter
*/
export const includeHugoFrontMatter = true;

/*
  Boolean flag to include original JSON data in the front matter
*/
export const includeOriginalJson = false;

/*
  Boolean flag to include referrers field in the front matter
*/
export const includeReferrers = false;

/*
  Boolean flag to always create section pages
*/
export const alwaysCreateSectionPages = false;

/*
  Mapping from Content Type or Taxonomy Vocabulary names to URL
*/
const name2url = {};

/*
  Content type names and taxonomy names to be removed
*/
const removedNames = [];

/*
  Section page hook
*/
export function sectionPageHook(dir, fileName, frontMatter, body, node) {
  // frontMatter.title = node.attributes.name;
  frontMatter.draft = node.attributes?.status ? false : true;
  if (removedNames.includes(frontMatter.name)) {
    return null;
  }
  // Set URL
  frontMatter.url = name2url[frontMatter.name];
  return { dir, fileName, frontMatter, body };
}

/*
  Single page hook
*/
export function singlePageHook(dir, fileName, frontMatter, body, node) {
  // frontMatter.title = node.attributes.title;
  // frontMatter.date = ???;
  frontMatter.date = node.attributes?.created;
  frontMatter.lastmod = node.attributes?.changed;
  // frontMatter.expiryDate = ???;
  if (node.attributes?.moderation_state != null) {
    frontMatter.draft = node.attributes.moderation_state != "published";
  } else if (node.attributes?.status != null) {
    frontMatter.draft = ! node.attributes.status;
  }
  frontMatter.promote = node.attributes?.promote;
  if (removedNames.includes(frontMatter.name)) {
    return null;
  }
  // Set URL
  if (frontMatter.url) {
    const url0 = name2url[frontMatter.name];
    if (url0 && ! frontMatter.url.startsWith(url0)) {
      frontMatter.url = url0 + frontMatter.url.replace(/\/?$/, "").replace(/^.*\//, "") + "/";
      console.error(`Fixed bad URL alias of node ${frontMatter.nid} to ${frontMatter.url}`);
    }
  } else {
    if (name2url[frontMatter.name]) {
      frontMatter.url = name2url[frontMatter.name] + frontMatter.nid + "/";
    }
  }
  // Create subsection pages except 'page'
  let subsections = Object.keys({ ...frontMatter.references, ...frontMatter.referrers });
  subsections = subsections.filter(name => name != "page");
  return { subsections, dir, fileName, frontMatter, body };
}

/*
  Subsection page hook
*/
export function subsectionPageHook(dir, fileName, frontMatter, body) {
  if (removedNames.includes(frontMatter.name)) {
    return null;
  }
  return { dir, fileName, frontMatter, body };
}

/*
  Taxonomy page hook
*/
export function taxonomyPageHook(dir, fileName, frontMatter, body, node) {
  // frontMatter.title = node.attributes.name;
  frontMatter.draft = node.attributes?.status ? false : true;
  if (removedNames.includes(frontMatter.name)) {
    return null;
  }
  // Set URL
  frontMatter.url = name2url[frontMatter.name];
  return { dir, fileName, frontMatter, body };
}

/*
  Term page hook
*/
export function termPageHook(dir, fileName, frontMatter, body, node) {
  // frontMatter.title = node.attributes.name;
  // frontMatter.date = ???;
  frontMatter.lastmod = node.attributes?.changed;
  // frontMatter.expiryDate = ???;
  frontMatter.draft = node.attributes?.status ? false : true;
  if (removedNames.includes(frontMatter.name)) {
    return null;
  }
  // Set URL
  if (frontMatter.url) {
    const url0 = name2url[frontMatter.name];
    if (url0 && ! frontMatter.url.startsWith(url0)) {
      frontMatter.url = url0 + frontMatter.url.replace(/\/?$/, "").replace(/^.*\//, "") + "/";
      console.error(`Fixed bad URL alias of term ${frontMatter.tid} to ${frontMatter.url}`);
    }
  } else {
    if (name2url[frontMatter.name]) {
      frontMatter.url = name2url[frontMatter.name] + frontMatter.tid + "/";
    }
  }
  return { dir, fileName, frontMatter, body };
}
