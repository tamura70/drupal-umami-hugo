#!/bin/sh
SRC="$1/sites/default/files"
DEST="static/sites/default/files"
if [ ! -e "$SRC" ]; then
    echo "Source directory $SRC does not exist"
    exit 1
fi
if [ ! -e "$DEST" ]; then
    mkdir -p "$DEST"
fi
rsync -arm --delete \
      --include "**/" \
      --include "*.jpg" \
      --include "*.jpeg" \
      --include "*.png" \
      --include "*.gif" \
      --exclude "*" \
      "$SRC"/ \
      "$DEST"
exit 0
